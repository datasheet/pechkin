<?php
/**
* Pechkin
* 
* @author KAD Systems (©) 2015	
* @date 20-02-2015	
* KME 1.1.2
*/
 
require_once('../../bootstrap.php');

$oModuleController = new Kad_Module_Controller(10);
$module_name = $oModuleController->get('internal_name');
$module_version = $oModuleController->get('version');
$titles = $oModuleController->get('client_name');
$site = $oModuleController->get('module_site');

$module_path = '/' . $module_name . '/';
$module_link = '/admin/'.$module_name.'/index.php';
$domain = $_SERVER['HTTP_HOST'];
$page_link = $_SERVER['SCRIPT_NAME'];

$sDomain = 0;
Core_Auth::authorization($module_name);
$action = Core_Array::getGet('action', ''); 
$site_id = CURRENT_SITE;
	
$oAdmin_Form_Controller = Admin_Form_Controller::create();
$oAdmin_Form_Controller
	->setUp()
	->path($module_link)
	->title($titles)
	->pageTitle($titles);
	
$sWindowId = $oAdmin_Form_Controller->getWindowId();

$aMessages = array();

// Авторизация
$oPechkin = Pechkin_Controller::instance();

if ($oAdmin_Form_Controller->getAction() == 'exit')
{
	$oPechkin->unlogin();
}

// Проверка авторизации
$auth = $oPechkin->checkAuth();
$authError = false;

// Логин
if (!$auth && Core_Array::getPost('login') && Core_Array::getPost('password'))
{
	if ($oPechkin->login(Core_Array::getPost('login'), Core_Array::getPost('password')))
	{
		$auth = true;
	}
	else
	{
		$authError = true;
	}
}


$oAdmin_View = Admin_View::create();

$oAdmin_View
	->module(Core_Module::factory($module_name))
	->pageTitle($titles);

ob_start();

$oAdmin_Form_Entity_Form = Admin_Form_Entity::factory('Form')
	->controller($oAdmin_Form_Controller)
	->action($module_link);
	
/*Admin_Form_Entity::factory('Title')
	->name($titles)
	->execute();
	*/
// Меню формы
$oAdmin_Form_Entity_Menus = Admin_Form_Entity::factory('Menus');

if ($auth)
{
	$oAdmin_Form_Entity_Menus->add(
		Admin_Form_Entity::factory('Menu')
			->name($oPechkin->login)
			->icon('fa fa-hand-o-right')
			->add(
				Admin_Form_Entity::factory('Menu')
					->icon('fa fa-power-off')
					->name("Выйти")
					->img('/admin/images/exit.gif')
					->href(
						$oAdmin_Form_Controller->getAdminLoadHref($module_link, 'exit', NULL, 0, 0)
					)
					->onclick(
						$oAdmin_Form_Controller->getAdminLoadAjax($module_link, 'exit', NULL, 0, 0)
					)
			)
	);
}

$oAdmin_Form_Entity_Menus
->add(
	Admin_Form_Entity::factory('Menu')
		->name('Экспорт')
		->icon('fa fa-file')
		->add(
			Admin_Form_Entity::factory('Menu')
				->name("Лог последней выгрузки")
				->icon("fa fa-file-text-o")
				->img('/admin/images/list.gif')
				->href(
					$oPechkin->log_file
				)
				->onclick(
					"window.open('" . $oPechkin->log_file . "'); return false;"
				)
		)->add(
			Admin_Form_Entity::factory('Menu')
				->name("Лог автоматической выгрузки")
				->icon("fa fa-file-text-o")
				->img('/admin/images/list.gif')
				->href(
					$oPechkin->log_auto_file
				)
				->onclick(
					"window.open('" . $oPechkin->log_auto_file . "'); return false;"
				)
		)
)
->add(
	Admin_Form_Entity::factory('Menu')
		->name('Помощь')
		->icon('fa fa-question')
		->add(
			Admin_Form_Entity::factory('Menu')
				->name("О модуле..")
				->icon("fa fa-bug icon-separator")
				->img('/admin/images/bug.gif')
				->href(
					$site
				)
				->onclick(
					"window.open('".$site."'); return false;"
				)
		)
)
;

// Добавляем все меню контроллеру
//$oAdmin_Form_Entity_Menus->execute();
$oAdmin_View->addChild($oAdmin_Form_Entity_Menus);


$oAdmin_Form_Entity_Tabs = Admin_Form_Entity::factory('Tabs');
$oAdmin_Form_Entity_Tabs->formId(123456789);

if (!$auth)
{
	if ($authError)
	{
		Core_Message::show("Неверный логин и/или пароль", 'error');
	}
	
	$oSite = Core_Entity::factory('site', CURRENT_SITE);
	$email = $oSite->admin_email;
		
	// Если неавторизован, выводим форму авторизации
	$oAdmin_Form_Entity_Form
		->add(
			Admin_Form_Entity::factory('Code')
				->html('')
		)
		->add(
			Admin_Form_Entity::factory('Input')
				->name('login')
				->id("login")
				->size(40)
				->caption("Логин на сервисе pechkin-mail.ru <a href='https://web.pechkin-mail.ru/login.php?forgot_username' target='_blank'>Восстановить</a>")
		)			
		->add(
			Admin_Form_Entity::factory('Password')
				->name('password')
				->id("pass")
				->size(40)
				->caption("Пароль на сервисе pechkin-mail.ru <a href='https://web.pechkin-mail.ru/login.php?forgot_password' target='_blank'>Восстановить</a>")
		)
		->add(
			Admin_Form_Entity::factory('Button')
				->name('button')
				->type('button')
				->value("Зарегистрироваться")
				->class('applyButton btn btn-success')
				//->onclick(" window.open('https://web.pechkin-mail.ru/common/auth_new.php?email={$email}&username={$email}&integration=hostcms&registration=1'); return false;")
				->onclick(" window.open('https://web.pechkin-mail.ru/registration4.php?ref=120210'); return false;")
		)
		->add(
			Admin_Form_Entity::factory('Button')
				->name('button')
				->type('button')
				->value("Войти")
				->class('applyButton btn btn-blue')
				->onclick($oAdmin_Form_Controller->getAdminSendForm('exec'))
		)
		->add(
			Admin_Form_Entity::factory('Code')
				->html('
				
				<hr>
				<div class="container" style="margin: 0">
					<div class="row">
						<h4><b>Лучший инструмент для e-mail маркетинга</b></h4>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p style="font-size: 12pt;">
								Красивые и эффективные письма для вашего бизнеса. Печкин-mail - это десятки тысяч довольных клиентов. Ведь у нас есть все, что необходимо для построения успешного и выгодного e-mail маркетинга
							</p>
						</div>
					</div>
					
					<div class="row">
						<h4><b>Создание рассылки за 15 минут</b></h4>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
							<img src="https://pechkin-mail.ru/redesign/images/feature1.png" class="img-responsive">
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
							<p style="font-size: 12pt;">
								Наш мастер создания красочных имейл рассылок работает четко и безотказно.
								С его помощью вы найдете точку приложения своим талантам и отправите письмо, которое будут читать с удовольствием.
								Вам не нужны ни дизайнер, ни верстальщик - весь процесс автоматизирован и требует от вас минимум усилий.
								Просто наслаждайтесь творчеством!
							</p>
						</div>
					</div>
					
					<div class="row">
						<h4><b>Заметный эффект уже через месяц после регистрации</b></h4>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
							<img src="https://pechkin-mail.ru/redesign/images/feature2.png" class="img-responsive">
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
							<p style="font-size: 12pt;">
								Мы постоянно помогаем нашим Пользователям совершенствовать свой имейл маркетинг. Работать над статистикой и доставкой. Пользуясь нашими советами из блога, базы знаний и полезных рассылок, вы поднимете свой уровень от новичка до уверенного пользователя всего за 30 дней.
							</p>
						</div>
					</div>
					
					<div class="row">
						<h4><b>Увеличение повторных продаж и лояльности ваших клиентов</b></h4>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
							<img src="https://pechkin-mail.ru/redesign/images/feature3.png" class="img-responsive">
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
							<p style="font-size: 12pt;">
								Мы создали для вас автоматические и триггерные рассылки. Их можно интегрировать с любым сайтом. Вам надо всего лишь один раз настроить "умные сценарии", и они будут работать на увеличение ваших продаж без обедов, выходных и усталости.
							</p>
						</div>
					</div>
					
					<div class="row">
						<h4><b>С нами вы экономите до 25% в год на емейл-рассылках</b></h4>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
							<img src="https://pechkin-mail.ru/redesign/images/feature4.png" class="img-responsive">
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
							<p style="font-size: 12pt;">
								У нас оптимальные цены. Мы работаем только в рублях. Наша гибкая система тарифов с оплатой на несколько месяцев вперед вас приятно удивит и позволит отлично сэкономить. Бесплатный доступ, неограниченный временем, прекрасно подойдет для редких рассылок по маленьким базам.
							</p>
						</div>
					</div> 
				</div>
				<br>
				')
				
		)
		->add(
			Admin_Form_Entity::factory('Button')
				->name('button')
				->type('button')
				->value("Зарегистрироваться")
				->class('applyButton btn btn-success')
				//->onclick(" window.open('https://web.pechkin-mail.ru/common/auth_new.php?email={$email}&username={$email}&integration=hostcms&registration=1'); return false;")
				->onclick(" window.open('https://web.pechkin-mail.ru/registration4.php?ref=120210'); return false;")
		)
		
		;
}
else
{
	$action_need_next_step = 0;
	$timeout = 1;
	$limit = PECHKIN_EXPORT_LIMIT;
	$action = $oAdmin_Form_Controller->getAction();
	
	$aAddFields = array(
			0 => "..",
			"name" => "Имя",
			"surname" => "Фамилия",
			"patronymic" => "Отчество",
			"company" => "Компания",
			"phone" => "Телефон",
			"fax" => "Факс",
			"website" => "Сайт",
			"icq" => "ICQ",
			"postcode" => "Почтовый индекс",
			"country" => "Страна",
			"cite" => "Город",
			"address" => "Адрес"
		);	

	$step = Core_Array::getGet('step');
	$offset = 0;//$step * $limit;
	$aResult = 0;
	
	// Сохраняем настройки
	if (strpos($action, "save") !== false)
	{
		$oPechkin->saveParams(str_replace("save_", "", $action));
		Core_Message::show('Настройки сохранены!');
	}

	// Действия
	if (
			$action == 'exportgroup' ||
			$action == 'exportmaillist' ||
			$action == 'exportorders'
		)
	{ 
		if (!$step)
		{
			$oPechkin->saveParams($action);
		}
	
		if ($action == 'exportgroup')
		{	// Экспортируем пользователей
		
			// Получаем выбранные группы
			$aGroups = array();
			$aoGroups = Core_Entity::factory('siteuser_group')->getAllBySite_id(CURRENT_SITE); 		
			
			foreach ($aoGroups as $oGroup)
			{
				if ($oPechkin->getParam('exportgroup_users_group_' . $oGroup->id))
				{
					$aGroups[] = $oGroup->id;
				}
			}
		
			if (count($aGroups))
			{
				$aoSiteusers = $oPechkin->getSiteusersByGroups($aGroups, $offset, $limit);
	
				$aResult = $oPechkin->processExport($aoSiteusers, $action, $step);
			}
			else
			{
				Core_Message::show("Выберите группы пользователей!", 'error');
			}
		}
		
		if ($action == 'exportmaillist')
		{	// Экспортируем подписчиков
		
			// Получаем выбранные рассылки
			$aMaillists = array();
			$aoMaillists = Core_Entity::factory('maillist')->getAllBySite_id(CURRENT_SITE); 		
			
			foreach ($aoMaillists as $oMaillist)
			{
				if ($oPechkin->getParam('exportmaillist_maillists_' . $oMaillist->id))
				{
					$aMaillists[] = $oMaillist->id;
				}
			}
		
			if (count($aMaillists))
			{
				$aoSiteusers = $oPechkin->getSiteusersByMaillists($aMaillists, $offset, $limit);
							
				$aResult = $oPechkin->processExport($aoSiteusers, $action, $step);
			}
			else
			{
				Core_Message::show("Выберите почтовые рассылки!", 'error');
			}		
		}
		
		if ($action == 'exportorders')
		{	// Экспортируем данные из заказов

			// Получаем выбранные магазины
			$aShops = array();
			$aoShops = Core_Entity::factory('shop')->getAllBySite_id(CURRENT_SITE); 		
			
			foreach ($aoShops as $oShop)
			{
				if ($oPechkin->getParam('exportorders_shop_' . $oShop->id))
				{
					$aShops[] = $oShop->id;
				}
			}		
					
			if (count($aShops))
			{
				// Получаем заказы магазина
				$aoOrder = $oPechkin->getOrdersByShops($aShops, $offset, $limit);
				// Создаем временных пользователей
				$aoSiteusers = $oPechkin->createTempSiteusers($aoOrder);
				
				$aResult = $oPechkin->processExport($aoSiteusers, $action, $step);
			}
			else
			{
				Core_Message::show("Выберите интернет-магазины!", 'error');
			}	
		}		
		
		if (count($aResult))
		{
			Core_Message::show($aResult['message'], $aResult['type']);
			$action_need_next_step = $aResult['action_need_next_step'];
			
			if ($action_need_next_step)
			{
				$step++;
				
				$function = '<script type="text/javascript"> function pechkin_next_step() {' . 
					 $oAdmin_Form_Controller->getAdminActionLoadAjax($oAdmin_Form_Controller->getPath(), $action, NULL, 0, 0, "step={$step}")
					. ' } sctm = setTimeout ("pechkin_next_step()", "' . $timeout * 1000 . '");</script>';
								
				$oAdmin_Form_Entity_Form
					->add(
						Admin_Form_Entity::factory('Code')
							->html($function)
					);	
			}
		}
	}

	if (!$action_need_next_step)
	{
		// Вкладки
		$oTabs = $oAdmin_Form_Entity_Tabs;

		$aoInfosystemsTabs = array();
		
		// > Экспорт пользователей 
		if (Core::moduleIsActive('siteuser'))
		{
			$oInfosystemsTab = Admin_Form_Entity::factory('Tab')	
				->name('infosystem')
				//->icon('fa fa-envelope-square')
				->caption("Экспорт пользователей")
				;
							
			// > Группы пользователей
			$inputName = "exportgroup_users_group_";
			$oSection = Admin_Form_Entity::factory('Section')->caption("Группы пользователей");
						
			$aoGroups = Core_Entity::factory('siteuser_group')->getAllBySite_id(CURRENT_SITE); 				
			
			foreach ($aoGroups as $oGroup)
			{
				$count = $oPechkin->getCountSiteusersByGroups(array($oGroup->id));

				$oSection->add(
						Admin_Form_Entity::factory('Checkbox')
							->name($inputName . $oGroup->id)
							->value($oPechkin->getParam($inputName . $oGroup->id))
							->caption($oGroup->name . " <a href='/admin/siteuser/group/list/index.php?siteuser_group_id={$oGroup->id}' onclick='$.openWindow({path: \"/admin/siteuser/group/list/index.php\",additionalParams: \"siteuser_group_id={$oGroup->id}\", windowId: \"{windowId}\"}); return false'>({$count})</a>")
					);
			}
			$oInfosystemsTab->add($oSection);
			// < Группы пользователей	
			
			// > Адресные базы
			$oSection = $oPechkin->getAddressBasesSection("exportgroup", $oAdmin_Form_Controller);
			$oInfosystemsTab->add($oSection);
			// < Адресные базы

			// > Дополнительные поля	
			$oSection = $oPechkin->getAdditionalFieldsSection("exportgroup", $oAdmin_Form_Controller);
			
			if(!is_null($oSection))
			{
				$oInfosystemsTab->add($oSection);
			}
			
			// < Дополнительные поля
			
			// > Автоматическая выгрузка
			$inputName = "exportgroup_auto";
			$oSection = Admin_Form_Entity::factory('Section')->caption("Автоматическая выгрузка");

			$oSection->add(
					Admin_Form_Entity::factory('Checkbox')
						->name($inputName)
						->value($oPechkin->getParam($inputName))
						->caption("Выгружать автоматически при добавлении пользователя")
				);
				
			$oInfosystemsTab->add($oSection);
			// < Автоматическая выгрузка
			
			$oInfosystemsTab->add(
					Admin_Form_Entity::factory('Button')
						->name('button')
						->type('button')
						->value("Сохранить настройки")
						->class('applyButton btn btn-success')
						->onclick($oAdmin_Form_Controller->getAdminSendForm('save_exportgroup', NULL, 1 /* Чтобы предотвратить передачу шага после выгрузки */ ))
				);
			
			$oInfosystemsTab->add(
					Admin_Form_Entity::factory('Button')
						->name('button')
						->type('button')
						->value("Экспортировать пользователей")
						->class('applyButton btn btn-blue')
						->onclick($oAdmin_Form_Controller->getAdminSendForm('exportgroup', NULL, 1))
				);
				
			$aoInfosystemsTabs[] = $oInfosystemsTab;

			// < Экспорт пользователей 
		
			// > Экспорт Подписчиков 
			if (Core::moduleIsActive('maillist'))
			{
				$oInfosystemsTab = Admin_Form_Entity::factory('Tab')	
								->name('infosystem')
								->caption("Экспорт подписчиков");
								
				// > Почтовые рассылки
				$inputName = "exportmaillist_maillists_";
				$oSection = Admin_Form_Entity::factory('Section')->caption("Почтовые рассылки");
				
				$aoMaillists= Core_Entity::factory('maillist')->getAllBySite_id(CURRENT_SITE); 				
				
				foreach ($aoMaillists as $oMaillist)
				{
					$count = $oPechkin->getCountSiteusersByMaillists(array($oMaillist->id));

					$oSection->add(
							Admin_Form_Entity::factory('Checkbox')
								->name($inputName . $oMaillist->id)
								->value($oPechkin->getParam($inputName . $oMaillist->id))
								->caption($oMaillist->name . " <a href='/admin/siteuser/group/list/index.php?siteuser_group_id={$oMaillist->id}' onclick='$.openWindow({path: \"/admin/maillist/fascicle/siteuser/index.php\",additionalParams: \"maillist_id={$oMaillist->id}\", windowId: \"{windowId}\"}); return false'>({$count})</a>")
						);
				}
				$oInfosystemsTab->add($oSection);
				// < Почтовые рассылки	
				
				// > Адресные базы
				$oSection = $oPechkin->getAddressBasesSection("exportmaillist", $oAdmin_Form_Controller);
				$oInfosystemsTab->add($oSection);
				// < Адресные базы

				// > Дополнительные поля	
				$oSection = $oPechkin->getAdditionalFieldsSection("exportmaillist", $oAdmin_Form_Controller);
				
				if(!is_null($oSection))
				{
					$oInfosystemsTab->add($oSection);
				}
				
				// < Дополнительные поля
				
				// > Автоматическая выгрузка
				$inputName = "exportmaillist_auto";
				$oSection = Admin_Form_Entity::factory('Section')->caption("Автоматическая выгрузка");

				$oSection->add(
						Admin_Form_Entity::factory('Checkbox')
							->name($inputName)
							->value($oPechkin->getParam($inputName))
							->caption("Выгружать автоматически при добавлении подписчика")
					);
					
				$oInfosystemsTab->add($oSection);
				// < Автоматическая выгрузка
				
				$oInfosystemsTab->add(
						Admin_Form_Entity::factory('Button')
							->name('button')
							->type('button')
							->value("Сохранить настройки")
							->class('applyButton btn btn-success')
							->onclick($oAdmin_Form_Controller->getAdminSendForm('save_exportmaillist', NULL, 1 /* Чтобы предотвратить передачу шага после выгрузки */ ))
					);
				
				$oInfosystemsTab->add(
						Admin_Form_Entity::factory('Button')
							->name('button')
							->type('button')
							->value("Экспортировать подписчиков")
							->class('applyButton btn btn-blue')
							->onclick($oAdmin_Form_Controller->getAdminSendForm('exportmaillist', NULL, 1))
					);
					
				$aoInfosystemsTabs[] = $oInfosystemsTab;
								
			} 
			// < Экспорт Подписчиков 
			
			// > Экспорт Заказов 
			if (Core::moduleIsActive('shop'))
			{
				$oInfosystemsTab = Admin_Form_Entity::factory('Tab')	
								->name('infosystem')
								->caption("Экспорт адресов из свойств заказов");
				
				// > Интернет-магазины
				$inputName = "exportorders_shop_";
				$oSection = Admin_Form_Entity::factory('Section')->caption("Интернет-магазины");

				$aoShops = Core_Entity::factory('shop')->getAllBySite_id(CURRENT_SITE); 				
				
				foreach ($aoShops as $oShop)
				{
					$count = $oPechkin->getCountOrdersByShops(array($oShop->id));
				
					$oSection->add(
							Admin_Form_Entity::factory('Checkbox')
								->name($inputName . $oShop->id)
								->value($oPechkin->getParam($inputName . $oShop->id))
								->caption($oShop->name . " <a href='/admin/shop/order/index.php?shop_id={$oShop->id}' onclick='$.openWindow({path: \"/admin/shop/order/index.php\",additionalParams: \"shop_id={$oShop->id}\", windowId: \"{windowId}\"}); return false'>({$count})</a>")
						);
				}
				
				$oInfosystemsTab->add($oSection);
				// < Интернет-магазины
				
				// > Адресные базы
				$oSection = $oPechkin->getAddressBasesSection("exportorders", $oAdmin_Form_Controller);
				$oInfosystemsTab->add($oSection);
				// < Адресные базы

				// > Дополнительные поля	
				$oSection = $oPechkin->getAdditionalFieldsSection("exportorders", $oAdmin_Form_Controller);
				
				if(!is_null($oSection))
				{
					$oInfosystemsTab->add($oSection);
				}
				// < Дополнительные поля
				
				// > Автоматическая выгрузка
				$inputName = "exportorders_auto";
				$oSection = Admin_Form_Entity::factory('Section')->caption("Автоматическая выгрузка");

				$oSection->add(
						Admin_Form_Entity::factory('Checkbox')
							->name($inputName)
							->value($oPechkin->getParam($inputName))
							->caption("Выгружать автоматически при добавлении заказа")
					);
					
				$oInfosystemsTab->add($oSection);
				// < Автоматическая выгрузка
				
				$oInfosystemsTab->add(
						Admin_Form_Entity::factory('Button')
							->name('button')
							->type('button')
							->value("Сохранить настройки")
							->class('applyButton btn btn-success')
							->onclick($oAdmin_Form_Controller->getAdminSendForm('save_exportorders', NULL, 1 /* Чтобы предотвратить передачу шага после выгрузки */ ))
					);
				
				$oInfosystemsTab->add(
						Admin_Form_Entity::factory('Button')
							->name('button')
							->type('button')
							->value("Экспортировать адреса из свойств заказов")
							->class('applyButton btn btn-blue')
							->onclick($oAdmin_Form_Controller->getAdminSendForm('exportorders', NULL, 1))
					);
					
				$aoInfosystemsTabs[] = $oInfosystemsTab;
								
			} 
			// < Экспорт Заказов 
		}
		else	
		{	
			Core_Message::show('Для работы модуля требуется наличие активного модуля "Пользователи сайта"', 'error');
		}		
		
		// Первой ставим вкладку с которой работаем
		if (strpos($action, "exportmaillist") !== false)
		{
			$oTabs->add($aoInfosystemsTabs[1]);
			unset($aoInfosystemsTabs[1]);
		}
		if (strpos($action, "exportorders") !== false)
		{
			$oTabs->add($aoInfosystemsTabs[2]);
			unset($aoInfosystemsTabs[2]);
		}
		
		// Остальные вкладки
		foreach ($aoInfosystemsTabs as $oInfosystemsTab)
		{
			$oTabs->add($oInfosystemsTab);
		}

		$oAdmin_Form_Entity_Form->add($oTabs);
	}
}

$oAdmin_Form_Entity_Form->execute();

$content = ob_get_clean();

ob_start();

$oAdmin_View
	->content($content)
	->show();

$oAdmin_Answer = Core_Skin::instance()->answer();

Core_Skin::instance()
	->answer()
	->ajax(Core_Array::getRequest('_', FALSE))
	->content(ob_get_clean())
	->title($titles)
	->message('') 
	->execute();

/*
$oAdmin_Answer = Core_Skin::instance()->answer();

$oAdmin_Answer
	->ajax(Core_Array::getRequest('_', FALSE))
	->content(ob_get_clean())
	->message('')
	->title($titles)
	->execute();
	
*/	