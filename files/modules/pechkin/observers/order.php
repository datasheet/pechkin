<?php

 defined('HOSTCMS') || exit('HostCMS: access denied.');
 
/**
* Оформление заказа
* 
* @author KAD Systems (©) 2015
* @date 21-03-2015	
*/

 class Pechkin_Observers_Order
 {
    static public function onAfterCreate($object, $operation)
    {
		$oModule = Core_Entity::factory('module')->getByPath('pechkin');
		$action = "exportorders";
		
		if ($oModule && $oModule->active)
		{
			$oPechkin = Pechkin_Controller::instance();

			if ($oPechkin->checkAuth() && $oPechkin->getParam($action . '_auto'))
			{
				$aoSiteusers = $oPechkin->createTempSiteusers(array($object));
				$aResult = $oPechkin->exportUsers($aoSiteusers, $action);
			}
		}
	}
}