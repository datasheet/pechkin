<?php

 defined('HOSTCMS') || exit('HostCMS: access denied.');
 
/**
* Добавление пользователя
* 
* @author KAD Systems (©) 2015
* @date 21-03-2015	
*/

 class Pechkin_Observers_Siteuser
 {
    static public function onAfterCreate($object, $operation)
    {
		$oModule = Core_Entity::factory('module')->getByPath('pechkin');
		$action = "exportgroup";
		
		if ($oModule && $oModule->active)
		{
			$oPechkin = Pechkin_Controller::instance();

			if ($oPechkin->checkAuth() && $oPechkin->getParam($action . '_auto'))
			{
				$aResult = $oPechkin->exportUsers(array($object), $action);
			}
		}
	}
}