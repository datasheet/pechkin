<?php

 defined('HOSTCMS') || exit('HostCMS: access denied.');
 
/**
* Подписка на рассылку
* 
* @author KAD Systems (©) 2015
* @date 21-03-2015	
*/

 class Pechkin_Observers_Maillist
 {
    static public function onAfterCreate($object, $operation)
    {
		$oModule = Core_Entity::factory('module')->getByPath('pechkin');
		$action = "exportmaillist";
		
		if ($oModule && $oModule->active)
		{
			$oPechkin = Pechkin_Controller::instance();

			if ($oPechkin->checkAuth() && $oPechkin->getParam($action . '_auto') && !$oPechkin->checkSended($object->siteuser))
			{
				$aResult = $oPechkin->exportUsers(array($object->siteuser), $action);
			}
		}
	}
}