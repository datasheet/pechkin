<?php
/**
 * Pechkin observers.
 * 
 * @author KAD Systems (©) 2015	
 * @date 21-03-2015	
 */
 
Core_Event::attach('siteuser.onAfterCreate', array('Pechkin_Observers_Siteuser', 'onAfterCreate'));
Core_Event::attach('shop_order.onAfterCreate', array('Pechkin_Observers_Order', 'onAfterCreate'));
Core_Event::attach('maillist_siteuser.onAfterCreate', array('Pechkin_Observers_Maillist', 'onAfterCreate'));
?>