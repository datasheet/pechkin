<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
* Pechkin_Module
* KME 1.1.2
*
* @author KAD Systems (©) 2015	
* @date 20-02-2015	
*/
 
class Pechkin_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '1.0.2';
	public $date = '2015-02-08';
	public $version_number = 2;	
	protected $_module_id = 10;	
	protected $_internal_name = "pechkin";	
	protected $_client_name = "Печкин-мейл";
	protected $_admin_name = "Печкин-мейл";		
	protected $_module_site;
	protected $_module_path;
	protected $_controller;
	protected $_forms;
	
	/*
	* Установка модуля
	*/
	public function install()
	{
		$this->_installSQL();
		$this->_injectObservers();
		$this->_installKadModule();
		$this->_setConstants(array("PECHKIN_EXPORT_LIMIT" => 100));
		$this->_installForms();
		$this->_setParams(
				array(
					
				)
			);
		
		$this->_message("Модуль \"{$this->_client_name}\" успешно установлен!");
	}
		
	/*
	* Удаление модуля
	*/
	public function uninstall()
	{
		$this->_unsetConstants();
		$this->_uninstallForms();
		$this->_uninstallSQL();
		$this->_message("Модуль \"{$this->_client_name}\" успешно удален!");
	}	

	// Установка форм ЦА
	protected function _installForms()
	{
		// Код, сформированный генератором
	
		//$this->_forms[] = $oAdminForm;
	}	
	
	// Установка наблюдателей
	protected function _injectObservers()
	{
		$obs_file = "modules/" . $this->_internal_name . "/observers.php";
		
		if (is_file(CMS_FOLDER . $obs_file))
		{
			$constring = '$fn = CMS_FOLDER . "' . $obs_file . '"; if (file_exists($fn)) require_once($fn);';
			
			$text = Core_File::read(CMS_FOLDER . 'bootstrap.php');
			if (strpos($text, $constring) == 0)
			{
				$fp = fopen( CMS_FOLDER . 'bootstrap.php', 'a');
				fwrite($fp, "\n\r" . $constring);
				fclose($fp);
			}
		}
	}	
	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->menu = array(
			array(
				'sorting' => 270,
				'block' => 2,
				'ico' => 'fa fa-envelope-square',
				'name' => $this->_client_name,
				'href' => "/admin/{$this->_internal_name}/index.php",
				'onclick' => "$.adminLoad({path: '/admin/{$this->_internal_name}/index.php'}); return false"
			)
		);
		
		$this->_module_path = CMS_FOLDER . "/modules/{$this->_internal_name}/";
		$this->_module_site = "http://www.hostcms.ru/shop/market/modules/exportimport/pechkin/";
		$this->_controller = new Kad_Module_Controller($this->_module_id);
	}
	
	// Удаление форм
	protected function _uninstallForms()
	{
		// Формы Центра Администрирования
		/*$oAdminForm = Core_Entity::factory('admin_form', $this->_controller->get('admin_form_1'));
		if ($oAdminForm)
		{	
			$oAdminForm->delete();
		}*/
	}
	
	protected function _installKadModule()
	{
		Kad_Module_Controller::install();
		$this->_controller = new Kad_Module_Controller($this->_module_id);
	}	
	
	// Импорт таблиц модуля
	protected function _installSQL()
	{
		if (file_exists($this->_module_path . 'install.sql'))
		{
			$query = Core_File::read($this->_module_path . 'install.sql');
			ob_start();
			Sql_Controller::instance()->execute($query);
			ob_get_clean();
		}
	}
	
	// Установка констант
	protected function _setConstants($aConstants = array())
	{
		$aConstants[strtoupper($this->_internal_name) . "_VERSION"] = $this->version_number;
	
		foreach ($aConstants as $key => $value)
		{
			// Константы
			$oConstant = Core_Entity::factory('constant')->getByName($key);
			if (!$oConstant)
			{
				$oConstant = Core_Entity::factory('constant');
				$oConstant->name = $key;
				$oConstant->value = $value;
				$oConstant->active = 1;
				$oConstant->save();
			}
		}
	}	
		
	protected function _setParams($aParams = array())
	{
		if (count($this->_forms))
		{
			$i = 1;
			foreach($this->_forms as $oForm)
			{
				$aParams['admin_form_' . $i] = $oForm->id;
				$i++;
			}
		}
		
		$aParams['version'] = $this->version;
		$aParams['version_number'] = $this->version_number;
		$aParams['internal_name'] = $this->_internal_name;
		$aParams['client_name'] = $this->_client_name;
		$aParams['admin_name'] = $this->_admin_name;
		$aParams['module_site'] = $this->_module_site;
		
		foreach ($aParams as $key => $value)
		{
			$this->_controller->set($key, $value);
		}	
	}
	
	protected function _uninstallSQL()
	{
		if (file_exists($this->_module_path . 'uninstall.sql'))
		{
			$query = Core_File::read($this->_module_path . 'uninstall.sql');
			if (!empty($query))
			{
				ob_start();
				Sql_Controller::instance()->execute($query);
				ob_get_clean();
			}
		}
	}
	
	// Вывести сообщение
	protected function _message($message)
	{
		echo "<div id='message'>{$message}</div>";
	}
	
	// Удаление констант
	protected function _unsetConstants($aConstants = array())
	{
		$aConstants[strtoupper($this->_internal_name) . "_VERSION"] = $this->version_number;
	
		foreach ($aConstants as $key => $value)
		{
			// Константы
			$oConstant = Core_Entity::factory('constant')->getByName($key);
			if ($oConstant)
			{
				$oConstant->delete();
			}
		}
	}	
	
	protected function _addWord($value)
	{
		$oWord = Core_Entity::factory('admin_word');	
		$oWord->save();
		$this->_setWordValues($oWord->id, $value);

		return $oWord;
	}
	
	protected function _setWordValues($admin_word_id, $lang1, $lang2 = '', $lang3 = '')
	{
		$oWordValue = Core_Entity::factory('admin_word_value');	
		$oWordValue->admin_word_id = $admin_word_id;
		$oWordValue->admin_language_id = 1;
		$oWordValue->name = $lang1;
		$oWordValue->save();
		
		$oWordValue = Core_Entity::factory('admin_word_value');	
		$oWordValue->admin_word_id = $admin_word_id;
		$oWordValue->admin_language_id = 2;
		$oWordValue->name = $lang2;
		$oWordValue->save();
		
		$oWordValue = Core_Entity::factory('admin_word_value');	
		$oWordValue->admin_word_id = $admin_word_id;
		$oWordValue->admin_language_id = 3;
		$oWordValue->name = $lang3;
		$oWordValue->save();		
	}	
}