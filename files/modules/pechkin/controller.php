<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
* Pechkin_Controller
* 
* @author KAD Systems (©) 2015	
* @date 20-02-2015	
*/

class Pechkin_Controller
{
	private $_controller = NULL;
	private $_pechkin = NULL;
	private $_module_id = 10;
	private $_aLists = array();
	
	private $_module_path = "/modules/pechkin/";	
	private $_log = array();
	
	private $_auth = false;	
	
	// Вести учет отправленных
	private $sendOnce = true;
	
	public $log_file = "log.txt";
	public $log_auto_file = "logauto.txt";
	public $login;
	public $pass;
	public $aAddFields = array(
			0 => "..",
			"name" => "Имя",
			"surname" => "Фамилия",
			"patronymic" => "Отчество",
			"company" => "Компания",
			"phone" => "Телефон",
			"fax" => "Факс",
			"website" => "Сайт",
			"icq" => "ICQ",
			"postcode" => "Почтовый индекс",
			"country" => "Страна",
			"cite" => "Город",
			"address" => "Адрес"
		);	
	
	/**
	 * The singleton instances.
	 * @var mixed
	 */
	static public $instance = NULL;

	/**
	 * Register an existing instance as a singleton.
	 * @return object
	 */
	static public function instance()
	{
		if (is_null(self::$instance))
		{
			self::$instance = new self();
		}

		return self::$instance;
	}
	
	public function __construct() 
	{
		$this->log_file = $this->_module_path . $this->log_file;
		$this->log_auto_file = $this->_module_path . $this->log_auto_file;
	
		$this->_controller = new Kad_Module_Controller($this->_module_id);
	}

	// Получение параметра
	public function getParam($param)
	{			
		if (Core_Array::getPost($param))
		{
			return Core_Array::getPost($param);
		}
		else
		{
			$param = CURRENT_SITE . "_" . $param;
			return $this->_controller->get($param);
		}
	}
	
	// Сохранение параметров по ключу
	public function saveParams($prefix)
	{		
		// Немного говнокода из-за проклятых чекбоксов
		// Сбрасываем значения всех параметров с данным ключом
		$oParams = Core_Entity::factory('kad_module_setting');
		$oParams->queryBuilder()
			->where('name', 'LIKE', $this->_module_id . "_" . CURRENT_SITE . "_" . $prefix . "%");
		$aoParams = $oParams->findAll();
		
		foreach ($aoParams as $oParam)
		{
			$oParam->value = "";
			$oParam->save();
		}
		
		// Сохраняем параметры
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, $prefix) !== false && !empty($value))
			{
				$this->setParam($key, $value);
			}
		}
		
		return true;
	}
	
	// Сохранение параметра
	public function setParam($param, $value)
	{		
		$param = CURRENT_SITE . "_" . $param;
		$this->_controller->set($param, $value);
	}	
	
	// Проверка авторизации
	public function checkAuth()
	{	
		if ($this->_auth)
		{
			return true;
		}
		
		$this->login = $this->getParam("p_login");
		$this->pass = base64_decode($this->getParam("p_password"));
		
		if ($this->login && $this->pass)
		{
			if ($this->checkAuthData($this->login, $this->pass))
			{
				return true;
			}
		}
		
		return false;
	}
	
	// Отправка запроса авторизации
	public function checkAuthData($login, $password)
	{
		$this->_pechkin = new Pechkin_Phpechkin($login, $password);

		if ($this->_pechkin->getData("lists.get") != 2)
		{
			$this->_auth = true;
			return true;
		}
		else
		{
			return false;
		}
	}

	// Логин
	public function login($login, $password)
	{
		if ($this->checkAuthData($login, $password))
		{
			$this->setParam("p_login", $login);
			$this->setParam("p_password", base64_encode($password));
			$this->login = $login;
			$this->pass = $password;
						
			return true;
		}
		else
		{
			return false;
		}
	}

	// Логин
	public function unlogin()
	{
		$this->setParam("p_login", "");
		$this->setParam("p_password", "");
	}	
	
	// Получить список баз
	public function getLists()
	{
		if (!count($this->_aLists))
		{
			$aLists = $this->_pechkin->getData("lists.get");
			$this->_aLists = is_array($aLists) ? $aLists : array();
		}
		
		return $this->_aLists;
	}
	
	// Получить id базы
	public function getList_id($type)
	{
		$aLists = $this->getLists();
		$list_id = $this->getParam($type . "_list_id");
		if (!$list_id)
		{
			@$list_id = $aLists['row'][0]['id'];
		}

		return $list_id;
	}	

	// Добавляем подписчика
	public function addMember($list_id, $email, $aMerge)
	{
		$result = $this->_pechkin->lists_add_member($list_id, $email, array(
				'merge_1' => $aMerge[1],
				'merge_2' => $aMerge[2],
				'merge_3' => $aMerge[3],
				'merge_4' => $aMerge[4],
				'merge_5' => $aMerge[5],
				//'update' => true
			));
		
		if (is_array($result))
		{	
			if (isset($result['member_id']))
			{
				return $result['member_id'];
			}
			else
			{
				return true;
			}
		}
		else
		{
			return $result;
		}
	}

	// Загрузка списка
	public function uploadList($list_id, $csv)
	{
		$exportFile = "modules/pechkin/pechkin.csv";
		Core_File::write(CMS_FOLDER . $exportFile, $csv);

		$result = $this->_pechkin->lists_upload($list_id, "http://" . getenv('HTTP_HOST') . "/" . $exportFile, 0, array(
				'merge_1' => 1,
				'merge_2' => 2,
				'merge_3' => 3,
				'merge_4' => 4,
				'merge_5' => 5,
				'type' => 'csv',
				//'update' => true
			));
	
		unlink(CMS_FOLDER . $exportFile);
		
		return $result;
	}	
	
	public function buildCSVLine($aParams, $separator = ",")
	{
		$result = "";
		
		foreach ($aParams as $param)
		{
			$result .= $param . $separator;
		}
		
		$result .= "\n";
		
		return $result;
	}
	
	// Название типа
	public function getTypeName($type, $many = false)
	{
		$type_name = "";
		
		switch ($type)
		{
			case 'exportmaillist':
				$type_name = ($many)?"подписчиков":"подписчика";
				break;
			case 'exportgroup':
				$type_name = ($many)?"пользователей":"пользователя";
				break;
			case 'exportorders':
				$type_name = ($many)?"данных из заказов":"заказа";
				break;				
		}
		
		return $type_name;
	}	
	
	// Экспортируем пользователей по одному
	public function exportUsers($aoSiteusers, $type)
	{
		$countMembersAdded = 0;
		$countMembersUpdated = 0;
		$countErrors = 0;
		
		$type_name = $this->getTypeName($type);
	
		if (count($aoSiteusers))
		{
			foreach ($aoSiteusers as $oSiteuser)
			{
				if ($type == 'exportorders')
				{
					$oEntity = Core_Entity::factory('shop_order', $oSiteuser->id);
				}
				else
				{
					$oEntity = $oSiteuser;
				}
				
				// Проверяем не был ли отправлен ранее
				if (!$this->checkSended($oEntity) && $oSiteuser->email)
				{
					$aMerge = array();
					$this->setSended($oEntity);
					
					for ($i = 1; $i <= 5; $i++)
					{
						$aSiteuser = $oSiteuser->toArray();
						
						$value = (isset($aSiteuser[$this->getParam($type . '_merge_' . $i)]))?$aSiteuser[$this->getParam($type . '_merge_' . $i)]:"";					
						$aMerge[$i] = $value;
					}
				
					$member_id = $this->addMember(
							$this->getParam($type . '_list_id'), 
							$oSiteuser->email,
							$aMerge
						);
						
					$userName = $oSiteuser->email;
					if (is_numeric($member_id))
					{
						$countMembersAdded++;
						$this->_log("Экспорт {$type_name} прошел успешно. Экспортирован {$userName}", true);
					}
					elseif ($member_id === true)
					{
						$countMembersUpdated++;
					}
					else
					{
						$countErrors++;
						$this->_log("Ошибка при экспорте {$userName}: {$member_id}", true);
					}
				}
			}
			
			return array(
					'countMembersAdded' => $countMembersAdded,
					'countMembersUpdated' => $countMembersUpdated,
					'countErrors' => $countErrors
				);
		}
	}	
	
	
	// Экспортируем пользователей списком
	public function exportUsersList($aoSiteusers, $type, $step)
	{		
		$membersList = "";
	
		if (count($aoSiteusers))
		{
			foreach ($aoSiteusers as $oSiteuser)
			{
				if ($oSiteuser->email)
				{
					$aMerge = array();
					
					for ($i = 1; $i <= 5; $i++)
					{
						$aSiteuser = $oSiteuser->toArray();
						
						$value = (isset($aSiteuser[$this->getParam($type . '_merge_' . $i)]))?$aSiteuser[$this->getParam($type . '_merge_' . $i)]:"";					
						$aMerge[$i] = $value;
					}
					
					$membersList .= $this->buildCSVLine(array($oSiteuser->email) + $aMerge);
				}
				
				if ($type == 'exportorders')
				{
					$oEntity = Core_Entity::factory('shop_order', $oSiteuser->id);
				}
				else
				{
					$oEntity = $oSiteuser;
				}
				
				$this->setSended($oEntity);
			}
			
			// Загрузка списка
			if (!empty($membersList))
			{
				$aResult = $this->uploadList($this->getParam($type . '_list_id'), $membersList);
				
				return $aResult;
			}
		}
	}	

	// Экспорт 
	public function processExport($aoSiteusers, $action, $step)
	{	
		$message = "";
		$type_name = $this->getTypeName($action, true);
		$answer_type = "";
		$action_need_next_step = false;
		$aResult = array();
		
		// Обнуляем лог на первом шаге
		if (!$step)
		{
			$this->_clearLogFile();
		}
		
		if (count($aoSiteusers))
		{
			$aResult = $this->exportUsersList($aoSiteusers, $action, $step);			

			if (count($aResult))
			{
				$text = $aResult['text'];
				$type = $aResult['type'];
				
				$answer_type = ($type == "errors")?"error":"message";

				$message = "Экспорт {$type_name} - шаг " . ($step+1) . ". " . $text;
							
				$action_need_next_step = true;
			}
		} 
		elseif($step)
		{
			$answer_type = "message";
			$message = "Экспорт {$type_name} завершен!";
		}
		
		if (!$step && !count($aResult))
		{
			$answer_type = "error";
			$message = "Нет данных для экспорта {$type_name}.";
		}
	
		$this->_log($message);
	
		return array(
				"type" => $answer_type,
				"message" => $message,
				"action_need_next_step" => $action_need_next_step
			);
	}
	
	// Отметить отправленным
	public function setSended($object)
	{	
		$object->pechkin_exported = 1;
		$object->save();
	}
	
	// Проверить был ли отправлен
	public function checkSended($object)
	{	
		return $object->pechkin_exported;
	}
	
	// Получаем количество пользователей по группам
	public function getCountOrdersByShops($aShops)
	{
		$oQB = Core_QueryBuilder::select(array('COUNT(*)', 'result'))
			->from('shop_orders')
			->where('shop_id', 'IN', $aShops)
			->where('siteuser_id', '=', 0)
			;
			
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oQB->where('pechkin_exported', '=', 0);
		}
		
		$aResult = $oQB->execute()->asAssoc()->result();
		
		if (isset($aResult[0]['result']))
		{
			return $aResult[0]['result'];
		}
		else
		{
			return 0;
		}
	}
	
	// Получаем заказы по магазинам
	public function getOrdersByShops($aShops, $offset = 0, $limit = 99999)
	{
		$oOrders = Core_Entity::factory('shop_order');
		$oOrders->queryBuilder()
			->where('shop_id', 'IN', $aShops)
			->where('siteuser_id', '=', 0)
			->limit($limit)
			->offset($offset)
			;
		
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oOrders->queryBuilder()
				->where('pechkin_exported', '=', 0)
				;
		}

		$aoOrders = $oOrders->findAll();

		return $aoOrders;
	}
	
	// Создаем временных пользователей по заказам
	public function createTempSiteusers($aoOrders)
	{
		$aoSiteusers = array();
		
		foreach ($aoOrders as $oOrder)
		{
			$oSiteuser = Core_Entity::factory('siteuser');
			$oSiteuser->name = $oOrder->name;
			$oSiteuser->surname = $oOrder->surname;
			$oSiteuser->patronymic = $oOrder->patronymic;
			$oSiteuser->phone = $oOrder->phone;
			$oSiteuser->email = $oOrder->email;
			$oSiteuser->address = $oOrder->address;
			$oSiteuser->id = $oOrder->id;
			
			$aoSiteusers[] = $oSiteuser;
		}
		
		return $aoSiteusers;
	}
	
	// Получаем количество пользователей по группам
	public function getCountSiteusersByGroups($aGroups)
	{
		$oQB = Core_QueryBuilder::select(array('COUNT(*)', 'result'))
			->from('siteusers')
			->join('siteuser_group_lists', 'siteusers.id', '=', 'siteuser_group_lists.siteuser_id')
			->where('siteuser_group_id', 'IN', $aGroups)
			;
			
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oQB->where('pechkin_exported', '=', 0);
		}
		
		$aResult = $oQB->execute()->asAssoc()->result();
		
		if (isset($aResult[0]['result']))
		{
			return $aResult[0]['result'];
		}
		else
		{
			return 0;
		}
	}
	
	
	// Получаем пользователей по группам
	public function getSiteusersByGroups($aGroups, $offset = 0, $limit = 999)
	{
		$oSiteusers = Core_Entity::factory('siteuser');
		$oSiteusers->queryBuilder()
			->join('siteuser_group_lists', 'siteusers.id', '=', 'siteuser_group_lists.siteuser_id')
			->where('siteuser_group_id', 'IN', $aGroups)
			->groupBy('siteusers.id')
			->limit($limit)
			->offset($offset)
			;
			
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oSiteusers->queryBuilder()
				->where('pechkin_exported', '=', 0)
				;
		}
		
		$aoSiteusers = $oSiteusers->findAll();
		
		return $aoSiteusers;
	}

	// Получаем количество пользователей по рассылкам
	public function getCountSiteusersByMaillists($aMaillists)
	{
		$oQB = Core_QueryBuilder::select(array('COUNT(*)', 'result'))
			->from('siteusers')
			->join('maillist_siteusers', 'siteusers.id', '=', 'maillist_siteusers.siteuser_id')
			->where('maillist_id', 'IN', $aMaillists)
			;
			
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oQB->where('siteusers.pechkin_exported', '=', 0)	;
		}
		
		$aResult = $oQB->execute()->asAssoc()->result();
		
		if (isset($aResult[0]['result']))
		{
			return $aResult[0]['result'];
		}
		else
		{
			return 0;
		}
	}
	
	// Получаем пользователей по рассылкам
	public function getSiteusersByMaillists($aMaillists, $offset = 0, $limit = 99999)
	{
		$oSiteusers = Core_Entity::factory('siteuser');
		$oSiteusers->queryBuilder()
			->join('maillist_siteusers', 'siteusers.id', '=', 'maillist_siteusers.siteuser_id')
			->where('maillist_id', 'IN', $aMaillists)
			->groupBy('siteusers.id')
			->limit($limit)
			->offset($offset)
			;
			
		if ($this->sendOnce)
		{
			// Учет отправленных 
			$oSiteusers->queryBuilder()
				->where('pechkin_exported', '=', 0)
				;
		}

		$aoSiteusers = $oSiteusers->findAll();
		
		return $aoSiteusers;
	}
	
	// Формируем секцию с базами подписчиков
	public function getAddressBasesSection($type, $oAdmin_Form_Controller)
	{
		$start = microtime(true); 
		$inputName = $type . "_list_id";
		$aLists = $this->getLists();
		
		$list_id = $this->getList_id($type);
		// var_dump($aLists);
		$aListItems = array();
		
		if(isset($aLists['row']))
		{
			if(isset($aLists['row']['id']))
			{
				$aListItems[$aLists['row']['id']] = $aLists['row']['name'];
			}
			else
			{
				foreach ($aLists['row'] as $aList)
				{
					$aListItems[$aList['id']] = $aList['name'];
					
				}
			}
		}
		
		$oSection = Admin_Form_Entity::factory('Section')->caption("Адресные базы в pechkin-mail.ru");
		
		$oSection->add(
				Admin_Form_Entity::factory('Select')
					->name($inputName)
					->caption("Адресная база")
					->options($aListItems)
					->value($list_id)
					->onchange($oAdmin_Form_Controller->getAdminSendForm('save_' . $type))
			);
			
		$oSection->add(
			Admin_Form_Entity::factory('div')
				->class('form-group col-lg-12 col-md-12 col-sm-12 col-xs-12')
				->add(
					Admin_Form_Entity::factory('Button')
						->name('button')
						->type('button')
						->class('applyButton btn btn-success')
						->value("Добавить адресную базу")
						->onclick(
						"window.open('https://web.pechkin-mail.ru/?page=lists'); return false;"
						)
				)
			);
		
		return $oSection;
	}
	
	// Формируем секцию с дополнительными полями
	public function getAdditionalFieldsSection($type, $oAdmin_Form_Controller)
	{
		$inputName = $type . "_merge_";
		$oSection = Admin_Form_Entity::factory('Section')->caption("Дополнительные поля в pechkin-mail.ru");
		
		$countFields = 0;
		$aLists = $this->getLists();
		$list_id = $this->getList_id($type);
		
		if(isset($aLists['row']))
		{
			if(isset($aLists['row']['id']))
			{
				$aList = $aLists['row'];
				
				if ($aList['id'] == $list_id)
				{
					for ($i = 1; $i <= 5; $i++)
					{
						$oField = Admin_Form_Entity::factory('Select')
							->name($inputName . $i)
							->options($this->aAddFields)
							;
									
						if ($aList['merge_' . $i])
						{
							$aField = unserialize($aList['merge_' . $i]);
							
							$oField->caption($aField['title']);
							
							if ($this->getParam($inputName . $i))
							{
								$oField->value($this->getParam($inputName . $i));
							}
							else
							{
								$oField->value(array_search($aField['title'], $this->aAddFields));
							}
							
							$countFields++;
						}
						else
						{
							$oField->divAttr(array('style' => 'display:none;'));
						}
						
						$oSection->add($oField);					
					}
				}
			}
			else
			{
				foreach ($aLists['row'] as $aList) 
				{
					if ($aList['id'] == $list_id)
					{
						for ($i = 1; $i <= 5; $i++)
						{
							$oField = Admin_Form_Entity::factory('Select')
								->name($inputName . $i)
								->options($this->aAddFields)
								;
										
							if ($aList['merge_' . $i])
							{
								$aField = unserialize($aList['merge_' . $i]);
								
								$oField->caption($aField['title']);
								
								if ($this->getParam($inputName . $i))
								{
									$oField->value($this->getParam($inputName . $i));
								}
								else
								{
									$oField->value(array_search($aField['title'], $this->aAddFields));
								}
								
								$countFields++;
							}
							else
							{
								$oField->divAttr(array('style' => 'display:none;'));
							}
							
							$oSection->add($oField);					
						}
					}
				}
			}
		}
		
		$oSection->add(
			Admin_Form_Entity::factory('div')
				->class('form-group col-lg-12 col-md-12 col-sm-12 col-xs-12')
				->add(
					Admin_Form_Entity::factory('Button')
						->name('button')
						->type('button')
						->value("Добавить дополнительное поле")
						->class('applyButton btn btn-success')
						->onclick(
						"window.open('https://web.pechkin-mail.ru/?page=lists&action=fields&id={$list_id}'); return false;"
						)
				)
		);
			
		//if ($countFields)
		//{
			return $oSection;
		//}
	}
	
	// Пишем в лог
	protected function _log($message, $auto_flag = false)
	{
		// Заглушим пока запись при каждом вызове
		$this->_log = array();
		$this->_log[date('d.m.y H:i:s')] = $message;
		$this->_saveLogFile($auto_flag);
		
		return $this;
	}
	
	// Сохраняем файл лога
	protected function _saveLogFile($auto_flag)
	{
		$log_text = "";
		$log_file_path = ($auto_flag)?$this->log_auto_file:$this->log_file;
		
		foreach($this->_log as $date => $message)
		{
			$log_text .= "[{$date}] {$message}" . "\n";
		}

		if (file_exists(CMS_FOLDER . $log_file_path))
		{
			$log_text = $log_text . Core_File::read(CMS_FOLDER . $log_file_path);
		}
		
		Core_File::write(CMS_FOLDER . $log_file_path, $log_text, 0644);
		
		return $this;
	}
	
	// Очищаем лог файл
	protected function _clearLogFile()
	{
		if (file_exists(CMS_FOLDER . $this->log_file))
		{
			unlink(CMS_FOLDER . $this->log_file);
		}
		
		return $this;
	}	

}
